class RoutesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :is_admin?, except: [:index, :show]
  before_action :set_route, only: [:show, :edit, :update, :destroy]

  # GET /routes
  # GET /routes.json
  def index
    @routes = Route.all
  end

  def reorder_morning_stops

    @route_id = params[:id]
    @route = Route.find(@route_id)
    #make sure route has beginning and end stop ids
    @route.morning_start_stop_id = @route.morning_start_stop_id || @route.stops.first.id
    @route.morning_end_stop_id = @route.morning_end_stop_id || @route.stops.last.id
    @route.afternoon_start_stop_id = @route.afternoon_start_stop_id || @route.stops.last.id
    @route.afternoon_end_stop_id = @route.afternoon_end_stop_id || @route.stops.first.id
    @route.save

    @stops = Stop.joins(:morning_students).where('students.morning_stop_id = stops.id and stops.route_id = ? and stops.id != ? and stops.id != ? ', @route_id, @route.morning_start_stop_id , @route.morning_end_stop_id).distinct.order('morning_pickup_number asc').readonly(false)

    #add beginning and end stop
    @stops << @route.morning_end_stop
    @stops.unshift(@route.morning_start_stop)
    

    @xmlified_stops = xmlify_stops @stops
    @xmlified_stops = @xmlified_stops.gsub '&', 'and'
    @xml_addresses = ERB::Util.url_encode(@xmlified_stops)

    #send request to reorder stops
    response = RestClient.post  "http://www.mapquestapi.com/directions/v2/optimizedroute?key=sOgPH3Z8rohrbjFAAkz2FbryVKAKuoQ8&xml=#{@xml_addresses}", {:params => {"xml" => 's'}}

    response =  JSON.parse response  
    reordered_stops = response["route"]["locationSequence"]  
    
    reordered_stops.each_with_index do |stop, index|
      @stops[stop].morning_pickup_number = index
      @stops[stop].save
    end
  
    @start_time = @route.morning_start_time
    
    #get first stops arrive time from route info
    @stops[0].morning_arrive_time = @route.morning_start_time
    @stops[0].save
    
    #update stop arrive times based on results from mapquest
    response["route"]["legs"].each_with_index do |leg, index|
      @stops[index+1].morning_arrive_time = @stops[index].morning_arrive_time + leg["time"].seconds + 90.seconds # time for students to get in bus
      @stops[index+1].save
    end
    
    self.reorder_afternoon_stops
    
    redirect_to "/routes/#{@route_id}/"
  end
  
    def reorder_afternoon_stops
    @route_id = params[:id]
    @route = Route.find(@route_id)
    @stops = Stop.joins(:afternoon_students).where('students.afternoon_stop_id = stops.id and stops.route_id = ? and stops.id != ? and stops.id != ? ', @route_id, @route.afternoon_start_stop_id , @route.afternoon_start_stop_id).distinct.order('afternoon_pickup_number asc').readonly(false)

    #add beginning and end stop

    @stops << @route.afternoon_end_stop
    @stops.unshift(@route.afternoon_start_stop)
    
    @xmlified_stops = xmlify_stops @stops
    @xmlified_stops = @xmlified_stops.gsub '&', 'and'
    @xml_addresses = ERB::Util.url_encode(@xmlified_stops)
    
    #send request to reorder stops
    response = RestClient.post  "http://www.mapquestapi.com/directions/v2/optimizedroute?key=sOgPH3Z8rohrbjFAAkz2FbryVKAKuoQ8&xml=#{@xml_addresses}", {:params => {"xml" => 's'}}

    response =  JSON.parse response  
    reordered_stops = response["route"]["locationSequence"]  
    
    reordered_stops.each_with_index do |stop, index|
      @stops[stop].afternoon_pickup_number = index
      @stops[stop].save
    end
  
    @start_time = @route.afternoon_start_time
    
    #get first stops arrive time from route info
    @stops[0].afternoon_arrive_time = @route.afternoon_start_time
    @stops[0].save
    
    response["route"]["legs"].each_with_index do |leg, index|
      @stops[index+1].afternoon_arrive_time = @stops[index].afternoon_arrive_time + leg["time"].seconds + 90.seconds # time for students to get in bus
      @stops[index+1].save
    end
    
  end
  
  # GET /routes/1
  # GET /routes/1.json
  def show
    @route_id = params[:id]
    @return_directions = params[:return]
    @route = Route.find(@route_id)
    #@stops = Stop.where(:route_id => @route_id).order('morning_pickup_number asc') # .limit(8)

    @stops = Stop.joins(:morning_students).where('students.morning_stop_id = stops.id and stops.route_id = ?', @route_id).distinct.order('morning_pickup_number asc')
    #add beginning and end stop
    @stops << @route.morning_end_stop
    @stops.unshift(@route.morning_start_stop)

    @return_stops = Stop.joins(:afternoon_students).where('students.afternoon_stop_id = stops.id and stops.route_id = ?', @route_id).distinct.order('afternoon_pickup_number asc')
    #add beginning and end stop
    @return_stops << @route.afternoon_end_stop
    @return_stops.unshift(@route.afternoon_start_stop)

    @student_count = 0
    @addresses = []
    @return_addresses = []

    @ungeocoded_stops = false

    @stops.each do |stop| 
      if stop.latitude.nil? or stop.longitude.nil?
        @ungeocoded_stops = true
      end
      @addresses << stop.address 
      @student_count += stop.morning_students.count
    end

    @address_arr = []
    @addresses.each do |addr|
      @address_arr << '"' + addr + '"'
    end
    unless @return_directions.nil?
      @address_arr = @address_arr.reverse
    end
    @addresses = @address_arr.join(',')
    @addresses = @addresses.gsub '&', 'and'
    
    @hash = Gmaps4rails.build_markers(@stops) do |stop, marker|
      marker.lat stop.latitude
      marker.lng stop.longitude
      infobox= stop.address + "<br /> "
      stop.morning_students.each do |stud| infobox = infobox + " - " +  stud.first_name + " " + stud.last_name + "<br />" end
      marker.infowindow infobox
    end
    
  end

  
  # GET /routes/new
  def new
    @route = Route.new
    @stop = @route.stops.build

  end

  # GET /routes/1/edit
  def edit
    @route_id = params[:id]
    @route = Route.find(@route_id)
    @route_stops_select = @route.stops.map{|s| [s.address, s.id ]}
  end

  # POST /routes
  # POST /routes.json
  def create
    @route = Route.new(route_params)

    respond_to do |format|
      if @route.save
        format.html { redirect_to @route, notice: 'Route was successfully created.' }
        format.json { render action: 'show', status: :created, location: @route }
      else
        format.html { render action: 'new' }
        format.json { render json: @route.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /routes/1
  # PATCH/PUT /routes/1.json
  def update
    respond_to do |format|
      if @route.update(route_params)
        format.html { redirect_to @route, notice: 'Route was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @route.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /routes/1
  # DELETE /routes/1.json
  def destroy
    @route.destroy
    respond_to do |format|
      format.html { redirect_to routes_url }
      format.json { head :no_content }
    end
  end

  private
    # little xml builder for mapquest routes api
    def xmlify_stops stops

      xml = "<route><locations>"
      stops.each do |stop|
 
        xml += "<location><latLng><lat>" + stop.latitude.to_s + "</lat><lng>" + stop.longitude.to_s + "</lng></latLng></location>"   
      end
      xml += "</locations></route>"  
      return xml
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_route
      @route = Route.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def route_params
      params.require(:route).permit(:name, :driver, :morning_start_time, :afternoon_start_time, :id, :morning_start_stop_id, :morning_end_stop_id, :afternoon_start_stop_id, :afternoon_end_stop_id, 
              stops_attributes:[:id, :route_id, :address, :morning_pickup_number, :afternoon_pickup_number, :morning_arrive_time, :afternoon_arrive_time, :_destroy
              ]
      )
    end
end
