class StudentsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :is_admin?, only: [:edit, :update, :destroy, :import, :create]
  before_action :set_student, only: [:show, :edit, :update, :destroy]
  helper_method :sort_column, :sort_direction
  
  # GET /students
  # GET /students.json
  def index
    @students = Student.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page => params[:page], per_page: params[:per_page])
    manual = params[:manual]
    respond_to do |format|
      format.html
        if params[:type] == "mailmerge"
          #if manual
            #@exp_students = Student.where("students.manual_morning_arrive_time = '' or students.manual_morning_arrive_time is null" )
          #else
            @exp_students = Student.all #where(:transported => true)
          #end
          format.csv { send_data @exp_students.to_csv }
        elsif params[:type] == "distance_calc"
          @exp_students = Student.all
          format.csv { send_data @exp_students.to_csv_for_distance_calc }
        else
          @exp_students = Student.all
          format.csv { send_data @exp_students.to_csv_for_update }
        end

      format.js
      
    end
  end

  def mapstudents

    @students = Student.all

    @hash = Gmaps4rails.build_markers(@students) do |student, marker|
      color = "FF0000"
      route_name = "N"      
      unless student.pickup_stop.nil?
        case student.pickup_stop_route_name 
        when "Bus #1"
          route_name = "B1"      
          color = "FF0000"
        when "Bus #2"
          color = "00FF00"
          route_name = "B2"
        when "Bus #3"
          route_name = "B3"
          color = "0000FF"
        when "Van #1"
          route_name = "V1"
          color = "FFF000"
        when "Van #2"
          route_name = "V2"
          color = "00FFF0"
        when "Van #3"
          route_name = "V3"
          color = "F000FF"
        else
          color = "000000"      
        end
      end
      
      marker.lat student.latitude
      marker.lng student.longitude
      marker.picture(:url => "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=#{route_name}|#{color}|000000", :width => 36, :height => 36)
      student_address = student.address || ''
      infobox= student_address + "<br /> "
      infobox = infobox + " - " +  student.first_name + " " + student.last_name 
      marker.infowindow infobox
    end
    
  end
  
  def import
    Student.import(params[:file])
    @stops = Stop.all
    @stops.each do |stop| 
      while stop.latitude.nil? or stop.longitude.nil?
        stop.address = stop.address + ' '
        stop.save    
      end
    end
    redirect_to students_path, notice: "Students imported."
  end

  # GET /students/1
  # GET /students/1.json
  def show
  end

  # GET /students/new
  def new
    @student = Student.new
  end

  # GET /students/1/edit
  def edit
    @student_id = params[:id]
    @student = Student.find(@student_id)
    @student_stops_select = Stop.all.order('route_id asc, morning_pickup_number asc').map{|s| [s.route_name + " - " +s.address, s.id ]}
  end

  # POST /students
  # POST /students.json
  def create
    @student = Student.new(student_params)

    respond_to do |format|
      if @student.save
        format.html { redirect_to @student, notice: 'Student was successfully created.' }
        format.json { render action: 'show', status: :created, location: @student }
      else
        format.html { render action: 'new' }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /students/1
  # PATCH/PUT /students/1.json
  def update
    respond_to do |format|
      if @student.update(student_params)
        format.html { redirect_to @student, notice: 'Student was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /students/1
  # DELETE /students/1.json
  def destroy
    @student.destroy
    respond_to do |format|
      format.html { redirect_to students_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def student_params
      params.require(:student).permit(:ssid, :first_name, :last_name, :grade, :phone, :transported, :date_enrolled, :guardian_name, :morning_stop_id, :afternoon_stop_id, :id, :address
      )
    end

    def sort_column
        Student.column_names.include?(params[:sort]) ? params[:sort] : "last_name"
    end

    def sort_direction
        %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
    end

end
