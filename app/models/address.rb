class Address < ActiveRecord::Base
  belongs_to :addressable, polymorphic: true
  geocoded_by :full_street_address   # can also be an IP address
	after_validation :geocache, :if => lambda{ |obj| obj.address_present? and (obj.address_line_1_changed? or obj.address_line_2_changed? or obj.city_changed? or obj.city_changed? or obj.state_changed? or obj.zip_changed?) } 
	#, if: ->(obj){ obj.address.present? and (obj.address_line_1_changed? or obj.address_line_2_changed? or obj.city_changed? or obj.city_changed? or obj.state_changed? or obj.zip_changed? )}         # auto-fetch coordinates

	def full_street_address
		#self.zip  #for now
		# [self.address_line_1, self.address_line_2, self.city, self.state, self.zip].join(" ")
	  self.address_line_1 + " " + self.address_line_2 + " " + self.city + ", " + self.state + " " +  self.zip
	end

	def geocache
    address = self.address.downcase
    result = Geocache.select('coordinates').where('lower(address) = ? ', "#{address}").first
    if result.blank? 
      coords =result
    else
      coords =result.coordinates.split(',')
    end

    if coords.blank?
      coords = Geocoder.coordinates(address)
      while coords.nil?
        Rails.logger.debug  ' sleeping for 2 sec to give api a break.....'
        sleep 2
        coords = Geocoder.coordinates(address)
      end

      geocache = Geocache.new
      geocache.address = address
      geocache.coordinates = coords.join(',')
      geocache.save
    end

    self.latitude = coords[0]
    self.longitude = coords[1]
    #self.save(:validate => false)
  end
end
