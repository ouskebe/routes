class Geocache < ActiveRecord::Base

	def self.geocode address
		
		  address = address.downcase
	    result = Geocache.select('coordinates').where('lower(address) = ? ', "#{address}").first
	    if result.blank? 
	      coords =result
	    else
	      coords =result.coordinates.split(',')
	    end

		  if coords.blank?
		    coords = Geocoder.coordinates(address)
        while coords.nil?
          Rails.logger.debug  ' sleeping for 2 sec to give api a break.....'
          sleep 2
          coords = Geocoder.coordinates(address)
        end

        distance = Geocache.get_distance_to_school(address)
        geocache = Geocache.new
        geocache.address = address
      	geocache.coordinates = coords.join(',')
        geocache.distance_to_school = distance
        geocache.save
		  end

		  coords
	end

  def self.get_distance_to_school(address)
    require 'httparty'
    require 'json'
    student_address = URI::escape(address)
    school_address = "2261%20South%20Hamilton%20Road,%20Columbus,%20OH%2043232"
    google_uri = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=#{student_address}&destinations=#{school_address}&units=imperial"
    response = HTTParty.get(google_uri)
    json = JSON.parse(response.body)
    distance_in_meters = json["rows"][0]["elements"][0]["distance"]["value"]
    distance_in_miles = distance_in_meters * 0.000621371
    return distance_in_miles.round(1)
  end
end


 