class Route < ActiveRecord::Base
  has_many :stops
  belongs_to :morning_start_stop, :class_name => "Stop", :foreign_key => "morning_start_stop_id"
  belongs_to :morning_end_stop, :class_name => "Stop", :foreign_key => "morning_end_stop_id"
  belongs_to :afternoon_start_stop, :class_name => "Stop", :foreign_key => "afternoon_start_stop_id"
  belongs_to :afternoon_end_stop, :class_name => "Stop", :foreign_key => "afternoon_end_stop_id"
  
  accepts_nested_attributes_for :stops, :reject_if => :all_blank, :allow_destroy => true

  def student_count
  	total_count = 0
  	self.stops.each do | stop|
  	  total_count += stop.morning_students.count	
  	end
  	total_count 
  end


  def get_or_create_stop (address)
    route_id = self.id
      coords = Geocache.geocode(address)
      # while coords.nil?
      #   Rails.logger.debug ' sleeping for 2 sec to give google api a break.....'
      #   sleep 2
      #   coords = Geocoder.coordinates(address)
      # end
      stop = Stop.find_by(:latitude => coords[0], :longitude => coords[1], :route_id => route_id) || Stop.new

      stop.route_id = route_id
      
      stop.address = address
      stop.morning_arrive_time = Time.now
      stop.afternoon_arrive_time = Time.now
      stop.save
      
      return stop
  end


end
