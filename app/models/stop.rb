class Stop < ActiveRecord::Base
  belongs_to :route
  has_many :morning_students, :class_name => "Student", :foreign_key => "morning_stop_id"
  has_many :afternoon_students, :class_name => "Student", :foreign_key => "afternoon_stop_id"
  
  geocoded_by :address   
	after_validation :geocache, :if => lambda{ |obj| !obj.address.blank? and obj.address_changed? } 

	  def route_name
    if self.route.nil? 
      ''
    else
      self.route.name
    end
  end

  def geocache
    address = self.address.downcase
    result = Geocache.select('coordinates').where('lower(address) = ? ', "#{address}").first
    if result.blank? 
      coords =result
    else
      coords =result.coordinates.split(',')
    end

    if coords.blank?
      coords = Geocoder.coordinates(address)
      while coords.nil?
        Rails.logger.debug ' sleeping for 2 sec to give api a break.....'
        sleep 2
        coords = Geocoder.coordinates(address)
      end

      geocache = Geocache.new
      geocache.address = address
      geocache.coordinates = coords.join(',')
      geocache.save
    end

    self.latitude = coords[0]
    self.longitude = coords[1]
    #self.save(:validate => false)
  end

end
