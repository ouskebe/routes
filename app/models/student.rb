class Student < ActiveRecord::Base
  belongs_to :stop
  belongs_to :pickup_stop, :class_name => "Stop", :foreign_key => "morning_stop_id"
  belongs_to :dropoff_stop, :class_name => "Stop", :foreign_key => "afternoon_stop_id"
   
  geocoded_by :address   
	after_validation :geocache, :if => lambda{ |obj| !obj.address.blank? and obj.address_changed? } 
	
  def self.search(search= nil)
    if search
      search = search.downcase
      where('lower(first_name) LIKE ? or lower(last_name) LIKE ?', "%#{search}%", "%#{search}%")
    else
      scoped
    end
  end

  def self.order_households
    students = Student.all.to_a
    i = 0
    while (i < students.count) do
      students[i].first_name = students[i].full_name 
      students = Student.compact_siblings(i, students) 
      i = i + 1  
    end
    return students
  end

  def self.compact_siblings(index, students) 
    i=0
    while (i < students.count) do
      if (i > index ) 
        if students[i].latitude ==  students[index].latitude and students[i].longitude ==  students[index].longitude
          students[index].first_name = students[index].first_name + ', ' + students[i].full_name
          students[index].grade = students[index].grade + ', ' + students[i].grade

          students[i] = nil
        end
      end
      i = i + 1 
    end
    students = students.compact
    return students
  end

  def pickup_stop_route_name
    if self.pickup_stop.nil? 
      ''
    elsif self.pickup_stop.route.nil?
      ''
    else
      self.pickup_stop.route.name
    end
  end

  def pickup_stop_address
    if self.pickup_stop.nil? 
      ''
    else
      self.pickup_stop.address
    end
  end

  def dropoff_stop_address
    if self.dropoff_stop.nil? 
      ''
    else
      self.dropoff_stop.address
    end
  end

  def pickup_stop_best_morning_arrive_time
    if self.pickup_stop.nil? 
      ''
    else
      self.pickup_stop.manual_morning_arrive_time.blank? ? self.pickup_stop_morning_arrive_time : self.pickup_stop.manual_morning_arrive_time.strftime("%I:%M %p")
    end
  end

  def dropoff_stop_best_afternoon_arrive_time
    if self.dropoff_stop.nil? 
      ''
    else
      self.dropoff_stop.manual_afternoon_arrive_time.blank? ? self.dropoff_stop_afternoon_arrive_time : self.dropoff_stop.manual_afternoon_arrive_time.strftime("%I:%M %p")
    end
  end

  def pickup_stop_morning_arrive_time
    if self.pickup_stop.nil? 
      ''
    else
      self.pickup_stop.morning_arrive_time.strftime("%I:%M %p")
    end
  end

  def dropoff_stop_afternoon_arrive_time
    if self.dropoff_stop.nil? 
      ''
    else
      self.dropoff_stop.afternoon_arrive_time.strftime("%I:%M %p")
    end
  end

  def full_name
    self.first_name + ' ' + self.last_name
  end

  def self.to_csv_for_update(options = {})
    columns = ["ssid", "last_name", "first_name", "grade", "transported", "route", "address", "phone", "date_enrolled", "guardian", "stop"]
    CSV.generate(options) do |csv|
      csv << columns
      all.each do |s|
        
          Rails.logger.debug s.id

        student_info = [s.ssid, s.last_name, s.first_name, s.grade, s.transported, s.pickup_stop_route_name, s.address, s.phone, s.date_enrolled, s.guardian_name, s.pickup_stop_address ]
        csv << student_info
      end
    end
  end

  def self.to_csv(options = {})
    columns = ["ssid", "full name", "grade", "transported", "less than 1 mile", "between 1 and 1.5 miles", "more than 1.5 miles", "route", "address", "phone", "date_enrolled", "guardian", "pickup address", "dropoff address", "pickup time", "dropoff time"]
    CSV.generate(options) do |csv|
      csv << columns
      Student.order_households.each do |s|
        distance = s.distance_to_school
        while distance.nil?
          s.address = s.address + ' '
          s.save
          distance = s.distance_to([39.927663, -82.879699]) 
        end
        logger.debug (s.inspect)
        less_than_1 = distance < 1 ? distance.round(2) : ''
        between_1_1_5 = (distance > 1 and distance < 1.5) ? distance.round(2) : ''
        more_than_1_5 = distance > 1.5 ? distance.round(2) : ''

        student_info = [s.ssid, s.first_name, s.grade, s.transported, less_than_1, between_1_1_5, more_than_1_5, s.pickup_stop_route_name, s.address, s.phone, s.date_enrolled, s.guardian_name, s.pickup_stop_address, s.dropoff_stop_address, s.pickup_stop_best_morning_arrive_time, s.dropoff_stop_best_afternoon_arrive_time ]
        csv << student_info
      end
    end
  end

  def self.to_csv_for_distance_calc(options = {})
    columns = ["ssid", "last name", "first_name", "grade", "transported", "less than 1 mile", "between 1 and 1.5 miles", "more than 1.5 miles", "route", "address", "phone", "date_enrolled", "guardian", "pickup address", "dropoff address", "pickup time", "dropoff time"]
    CSV.generate(options) do |csv|
      csv << columns
      Student.all.each do |s|
        distance = s.distance_to_school
        attempts = 0
        while distance.nil? and attempts < 3
          attempts = attempts + 1
          s.address = s.address + ' '
          s.save
          distance = s.distance_to([39.927663, -82.879699]) 
        end
        logger.debug (s.inspect)
        unless distance.nil?
          less_than_1 = distance < 1 ? distance.round(2) : ''
          between_1_1_5 = (distance > 1 and distance < 1.5) ? distance.round(2) : ''
          more_than_1_5 = distance > 1.5 ? distance.round(2) : ''
        end
        student_info = [s.ssid, s.last_name, s.first_name, s.grade, s.transported, less_than_1, between_1_1_5, more_than_1_5, s.pickup_stop_route_name, s.address, s.phone, s.date_enrolled, s.guardian_name, s.pickup_stop_address, s.dropoff_stop_address, s.pickup_stop_best_morning_arrive_time, s.dropoff_stop_best_afternoon_arrive_time ]
        csv << student_info
      end
    end
  end

  def self.import(file)

    Student.all.each do |s| s.destroy end
    Stop.all.each do |s| s.destroy end
    Route.all.each do |r| r.destroy end

    connection = ActiveRecord::Base.connection; 
    connection.execute('ALTER SEQUENCE students_id_seq RESTART WITH 1000') 
    connection.execute('ALTER SEQUENCE stops_id_seq RESTART WITH 1000') 
    connection.execute('ALTER SEQUENCE routes_id_seq RESTART WITH 1000') 

    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    student_arr =[]
    spreadsheet.each(:ssid => 'ssid', :first_name => 'first_name', :last_name => 'last_name', :address => 'address', :grade => 'grade', :transported => 'transported',
    :phone => 'phone', :route => "route", :stop => 'stop', :date_enrolled => 'date_enrolled', :guardian_name => 'guardian') {|hash| student_arr << hash}
    
    #remove header
    student_arr.shift
    i=0
    geocode_misses = 0
    student_arr.each do |stud|

      i += 1
      puts 
      if stud[:ssid].nil?
        student = new
      else
        student = find_by(ssid: stud[:ssid]) || new
      end

      #create stops
      unless stud[:stop].nil? or stud[:stop].blank? 
      
        #get route info
        unless stud[:route].nil? or stud[:route].blank?
          route = Route.find_by(:name => stud[:route]) || Route.new
          route.name = stud[:route]
          route.morning_start_time = '2000-01-01 07:15:00 UTC'
          route.afternoon_start_time = '2000-01-01 15:45:00 UTC'
          route.save

          @morning_start_stop = route.get_or_create_stop('4606 Heaton rd Columbus oh, 43229')
          @morning_end_stop = route.get_or_create_stop('2261 s hamilton rd Columbus oh, 43232')
          @afternoon_start_stop = route.get_or_create_stop('2261 s hamilton rd Columbus oh, 43232')
          @afternoon_end_stop = route.get_or_create_stop('4606 Heaton rd Columbus oh, 43229')

          route.morning_start_stop_id = @morning_start_stop.id
          route.morning_end_stop_id = @morning_end_stop.id
          route.afternoon_start_stop_id = @afternoon_start_stop.id
          route.afternoon_end_stop_id = @afternoon_end_stop.id
          route.save
        end
        route_id = route.nil? ? nil : route.id
        coords = Geocache.geocode(stud[:stop])
        # while coords.nil?
        #   geocode_misses +=1
        #   Rails.logger.debug stud[:id] + ' sleeping for 1 sec to give google api a break.....'
        #   sleep 2
        #   coords = Geocoder.coordinates(stud[:stop])
        # end
        stop = Stop.find_by(:latitude => coords[0], :longitude => coords[1], :route_id => route_id) || Stop.new
        
        stop.route_id = route.nil? ? nil : route.id
        
        stop.address = stud[:stop]
        stop.morning_arrive_time = Time.now
        stop.afternoon_arrive_time = Time.now
        stop.save
        
        student.morning_stop_id = stop.id  
        student.afternoon_stop_id = stop.id 
      end 
      
      stud.delete(:stop)
      stud.delete(:route)
      stud.delete(:id)
      stud[:transported] = stud[:transported].try(:strip).try(:downcase) == 'Yes'.downcase ? true : false
      student.attributes = stud
      student.save!
      # if i % 9 == 0
      #     sleep 3
      # end
      logger.debug student.first_name
    end

  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv" then Roo::CSV.new(file.path)
    when ".xls" then Roo::Excel.new(file.path)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

 
  def geocache
    address = self.address.downcase
    result = Geocache.select('coordinates, distance_to_school').where('lower(address) = ? ', "#{address}").first
    if result.blank? 
      coords =result
      distance = nil
    else
      coords =result.coordinates.split(',')
      distance = result.distance_to_school
    end
    

    if coords.blank?
      coords = Geocoder.coordinates(address)
      while coords.nil?
        Rails.logger.debug  ' sleeping for 2 sec to give api a break.....'
        sleep 2
        coords = Geocoder.coordinates(address)
      end

      distance = Geocache.get_distance_to_school(address)
      geocache = Geocache.new
      geocache.address = address
      geocache.distance_to_school = distance
      geocache.coordinates = coords.join(',')
      geocache.save

    end

    self.latitude = coords[0]
    self.longitude = coords[1]
    self.distance_to_school = distance
    #self.save(:validate => false)
  end

  private 

end
