json.array!(@routes) do |route|
  json.extract! route, :id, :name, :driver, :morning_start_time, :afternoon_start_time
  json.url route_url(route, format: :json)
end
