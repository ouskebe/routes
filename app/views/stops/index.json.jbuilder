json.array!(@stops) do |stop|
  json.extract! stop, :id, :route_id, :morning_pickup_number, :afternoon_pickup_number, :morning_arrive_time, :afternoon_arrive_time
  json.url stop_url(stop, format: :json)
end
