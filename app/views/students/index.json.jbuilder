json.array!(@students) do |student|
  json.extract! student, :id, :first_name, :last_name, :grade, :phone, :transported, :route_id, :date_enrolled, :guardian_name, :stop_id
  json.url student_url(student, format: :json)
end
