class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :first_name
      t.string :last_name
      t.string :grade
      t.string :phone
      t.boolean :transported
      t.integer :route_id
      t.date :date_enrolled
      t.string :guardian_name
      t.integer :morning_stop_id

      t.timestamps
    end
  end
end
