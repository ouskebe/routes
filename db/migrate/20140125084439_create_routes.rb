class CreateRoutes < ActiveRecord::Migration
  def change
    create_table :routes do |t|
      t.string :name
      t.string :driver
      t.time :morning_start_time
      t.time :afternoon_start_time

      t.timestamps
    end
  end
end
