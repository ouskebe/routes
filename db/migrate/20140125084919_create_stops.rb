class CreateStops < ActiveRecord::Migration
  def change
    create_table :stops do |t|
      t.integer :route_id
      t.integer :morning_pickup_number
      t.integer :afternoon_pickup_number
      t.time :morning_arrive_time
      t.time :afternoon_arrive_time

      t.timestamps
    end
  end
end
