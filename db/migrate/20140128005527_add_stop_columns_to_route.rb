class AddStopColumnsToRoute < ActiveRecord::Migration
  def change
      add_column :routes, :morning_start_stop_id, :integer
      add_column :routes, :morning_end_stop_id, :integer
      add_column :routes, :afternoon_start_stop_id, :integer
      add_column :routes, :afternoon_end_stop_id, :integer
                        
  end
end
