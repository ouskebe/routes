class AddAfternoonStopIdColumnToStudent < ActiveRecord::Migration
  def change
    add_column :students, :afternoon_stop_id, :integer
  end
end
