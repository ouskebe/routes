class AddAddressToStop < ActiveRecord::Migration
  def change
    add_column :stops, :address, :string
  end
end
