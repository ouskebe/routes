class CreateGeocaches < ActiveRecord::Migration
  def change
    create_table :geocaches do |t|
      t.string :address
      t.string :coordinates

      t.timestamps
    end
  end
end
