class AddColumnsToStop < ActiveRecord::Migration
  def change
  	add_column :stops, :manual_morning_arrive_time, :time
    add_column :stops, :manual_afternoon_arrive_time, :time
  end
end
