class AddDistanceToSchoolToStudent < ActiveRecord::Migration
  def change
    add_column :students, :distance_to_school, :float
  end
end
