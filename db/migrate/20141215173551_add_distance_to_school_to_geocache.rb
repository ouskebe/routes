class AddDistanceToSchoolToGeocache < ActiveRecord::Migration
  def change
    add_column :geocaches, :distance_to_school, :float
  end
end
