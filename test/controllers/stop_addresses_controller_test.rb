require 'test_helper'

class StopAddressesControllerTest < ActionController::TestCase
  setup do
    @stop_address = stop_addresses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:stop_addresses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create stop_address" do
    assert_difference('StopAddress.count') do
      post :create, stop_address: { address_line_1: @stop_address.address_line_1, address_line_2: @stop_address.address_line_2, city: @stop_address.city, state: @stop_address.state, zip: @stop_address.zip }
    end

    assert_redirected_to stop_address_path(assigns(:stop_address))
  end

  test "should show stop_address" do
    get :show, id: @stop_address
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @stop_address
    assert_response :success
  end

  test "should update stop_address" do
    patch :update, id: @stop_address, stop_address: { address_line_1: @stop_address.address_line_1, address_line_2: @stop_address.address_line_2, city: @stop_address.city, state: @stop_address.state, zip: @stop_address.zip }
    assert_redirected_to stop_address_path(assigns(:stop_address))
  end

  test "should destroy stop_address" do
    assert_difference('StopAddress.count', -1) do
      delete :destroy, id: @stop_address
    end

    assert_redirected_to stop_addresses_path
  end
end
