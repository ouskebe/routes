require 'test_helper'

class StudentAddressesControllerTest < ActionController::TestCase
  setup do
    @student_address = student_addresses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:student_addresses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create student_address" do
    assert_difference('StudentAddress.count') do
      post :create, student_address: { address_line_1: @student_address.address_line_1, address_line_2: @student_address.address_line_2, city: @student_address.city, state: @student_address.state, student_id: @student_address.student_id, zip: @student_address.zip }
    end

    assert_redirected_to student_address_path(assigns(:student_address))
  end

  test "should show student_address" do
    get :show, id: @student_address
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @student_address
    assert_response :success
  end

  test "should update student_address" do
    patch :update, id: @student_address, student_address: { address_line_1: @student_address.address_line_1, address_line_2: @student_address.address_line_2, city: @student_address.city, state: @student_address.state, student_id: @student_address.student_id, zip: @student_address.zip }
    assert_redirected_to student_address_path(assigns(:student_address))
  end

  test "should destroy student_address" do
    assert_difference('StudentAddress.count', -1) do
      delete :destroy, id: @student_address
    end

    assert_redirected_to student_addresses_path
  end
end
